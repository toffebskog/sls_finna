    function loadTranscriptionFor(id) {

        var audiofile = $("#" + id + " .sidebarcontent audio source").attr("src");
        if (audiofile) {
            var urlbase = "/dev4/themes/custom/files/xml/";
            var xmlfile = urlbase + audiofile.split("/").pop().replace(".mp3", ".xml");
            loadXmlFile(xmlfile, addTranscription);
        }
    }
    
    function loadXmlFile(xmlpath, callback) {
        $.ajax({
            type: "GET",
            url: xmlpath,
            dataType: "xml",
            success: callback
        });
    }
    
    function addTranscription(xml) {
        var $xml = $(xml);
        var title = $xml.find("COL:nth-child(2) DATA").text();
        var original = $xml.find("COL:nth-child(3) DATA").text().replace(/\[([0-9]+)\]/gim, '[<a href="#note$1" class="notelink" data-noteid="$1">$1</a>]').replace(/(?:\r\n|\r|\n)/g, '<br>');
        var translation = $xml.find("COL:nth-child(4) DATA").text().replace(/\[([0-9]+)\]/gim, '[<a href="#note$1" class="notelink" data-noteid="$1">$1</a>]').replace(/(?:\r\n|\r|\n)/g, '<br>');
        var glossary = $xml.find("COL:nth-child(5) DATA").text().replace(/\[([0-9]+)\](.*)/gim, '<a name="note$1"></a><span id="note-$1" class="gloss">[$1] $2</span>').replace(/(?:\r\n|\r|\n)/g, '<br>');
        
        $output = $('<div class="transcription"><h3></h3><div class="original"></div><div class="translation"></div><div class="glossary"></div></div>')
        $output.find("h3").html(title);
        $output.find(".original").html(original);
        $output.find(".translation").html(translation);
        $output.find(".glossary").html(glossary);
        
        $output.find(".notelink").each(function() {
            var note_id = $(this).data("noteid");
            $(this).attr("title", $output.find("#note-" + note_id).text());
        });
        

        $("#transcription").html($output);
    }